import stripe
import json
import pymongo
import configparser

from flask import Flask, render_template, request, jsonify

config = configparser.ConfigParser()
config.read('config.ini')


stripe.api_key = config["stripe_key"]["key"]
print(stripe.api_key)

myclient = pymongo.MongoClient("mongodb://{}:{}/".format(config["mongodb"]["ip"], config["mongodb"]["port"]))
print(myclient)
mydb = myclient["mydatabase"]
pm = mydb["paymentMethod"]
cus = mydb["customer"]
subs = mydb["subscription"]

app = Flask(__name__)

@app.route('/create-payment-method', methods=['POST'])
def create_payment_method():

    try:
        data = json.loads(request.data)
        paymentId = stripe.PaymentMethod.create(
            type="card",
            card={
                "number": data["number"],
                "exp_month": data["exp_month"],
                "exp_year": data["exp_year"],
                "cvc": data["cvc"],
            },
        )

        # update database
        try:
            if paymentId:
                x = pm.insert_one(paymentId)
        except:
            pass
        return({"result": "success", "paymentId": paymentId["id"]})
    except Exception as e:
        print(str(e))
        return jsonify(error=str(e)), 403


@app.route('/create-customer', methods=['POST'])
def create_customer():

    data = json.loads(request.data)
    try:

        customer = stripe.Customer.create(
            email=data['email'],
        )

        # update database
        try:
            if customer:
                x = cus.insert_one(customer)
        except:
            pass
        return({"result": "success", "customerId": customer["id"]})
    except Exception as e:
        print(str(e))
        return jsonify(error=str(e)), 403

@app.route('/attach-payment-method', methods=['POST'])
def attach_payment_method():

    data = json.loads(request.data)
    try:

        update = stripe.PaymentMethod.attach(
                data['payment_method_id'],
                customer=data['customer_id'],
        )

        customer_update = stripe.Customer.modify(
            data['customer_id'],
            invoice_settings={
                'default_payment_method': data['payment_method_id']
            }
        )
        if update:
            # update cusomter data in mongodb
            pass

        return({"result": "success"})
    except Exception as e:
        print(str(e))
        return jsonify(error=str(e)), 403

@app.route('/create-subscription', methods=['POST'])
def create_subscription():

    data = json.loads(request.data)
    customer_id = data['customer_id']
    try:
        subscription = stripe.Subscription.create(
            customer=data['customer_id'],
            items=[
                {
                    "price": data["plan_id"],
                },
            ],
            expand=["latest_invoice.payment_intent"]
        )

        return jsonify({"result": "success", "subscriptionId": subscription["id"]})
    except Exception as e:
        print(str(e))
        return jsonify(error=str(e)), 403


@app.route('/webhook', methods=['POST'])
def webhook_received():

    webhook_secret = os.getenv('STRIPE_WEBHOOK_SECRET')
    request_data = json.loads(request.data)

    if webhook_secret:
        signature = request.headers.get('stripe-signature')
        try:
            event = stripe.Webhook.construct_event(
                payload=request.data, sig_header=signature, secret=webhook_secret)
            data = event['data']
        except Exception as e:
            return e
        event_type = event['type']
    else:
        data = request_data['data']
        event_type = request_data['type']

    data_object = data['object']

    if event_type == 'invoice.payment_succeeded':
        print(data)

    if event_type == 'invoice.payment_failed':
        print(data)

    if event_type == 'customer.subscription.created':
        print(data)

    return jsonify({'status': 'success'})


if __name__ == '__main__':
    app.run(port=5000)