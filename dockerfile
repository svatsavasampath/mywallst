FROM python:3.8-slim

RUN mkdir /app
ADD app.py /app/
ADD requirements.txt /app/
RUN pip3 install -r /app/requirements.txt
WORKDIR /app/

EXPOSE 5000

#ENTRYPOINT ["python", "/app/app.py"]
ENTRYPOINT ["flask", "run", "--host", "0.0.0.0"]