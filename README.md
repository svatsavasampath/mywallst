# MyWallSt Subscription Stripe REST API

Rest API for setting up a subscription for customers

The module uses the Flask REST framework for building API which can be consumed by web, android, and IOS apps

MongoDB is used as a datastore.

# Usecase
1. create products
2. create price for the product
3. create customer
4. create payment methods
5. attach payment method to the customer and set default
6. create subscription
7. listen to stripe rest API calls for payments and update the internal system like enable/disable service

~ refer setup.py for workflow and research on stripe workflow done


## Installation

create python virtual env 
```bash
>pip install -r requirements.txt
>python setup.py # creates plan
```

## Start REST server


```bash
>python app.py
```

## Test Cases

```bash
>py.test
```

## (or) use Dockerfile for setup
configure mongoip in config.ini
```
docker build -t mywallst_subscription .
docker run -p 5000:5000 mywallst_subscription
```
## curl commands

### 1. create customer

curl -X POST -H 'Content-Type: application/json' -d '{
    "email": "vat@gmail.com"
}' http://127.0.0.1:5000/create-customer

### 2. create payment method

curl -X POST -H 'Content-Type: application/json' -d '{
    "number": "4242424242424242",
    "exp_month": 12,
    "exp_year": 2021,
    "cvc": "314"
}' http://127.0.0.1:5000/create-payment-method

### 3. Attach payment method to customer 

curl -X POST -H 'Content-Type: application/json' -d '{
    "payment_method_id": "pm_1I0F1RLptZvnIfh1l015lrr9",
    "customer_id": "cus_IbRvbMA5rawp1o"
}' http://127.0.0.1:5000/attach-payment-method

### 4. create subscription

curl -X POST -H 'Content-Type: application/json' -d '{
    "plan_id": "plan_IbRkRcsOk7wdEq",
    "customer_id": "cus_IbRvbMA5rawp1o"
}' http://127.0.0.1:5000/create-subscription


## TODO

1. database connection use singleton design pattern
2. add logging
3. add versioning for APIs
4. move each API to a different file for easy handing
5. add security key for API call for security purpose
6. add certificate https



## License
[MIT](https://choosealicense.com/licenses/mit/)