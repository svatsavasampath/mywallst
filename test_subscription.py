import requests
import json


URL = "http://127.0.0.1:5000/"

def test_create_payment_method():
    url = URL+'create-payment-method'
    print(url)
    headers = {'Content-Type': 'application/json' } 

    payload = {
    "number": "4242424242424242",
    "exp_month": 12,
    "exp_year": 2021,
    "cvc": "314",
  	}
    
    resp = requests.post(url, headers=headers, data=json.dumps(payload,indent=4))       
    
    assert resp.status_code == 200
    resp_body = resp.json()    
    print(resp_body, resp.text)

def test_create_customer():
    url = URL+'create-customer'
    print(url)
    headers = {'Content-Type': 'application/json' } 

    payload = {
    "email": "vat@gmail.com",
  	}
    
    resp = requests.post(url, headers=headers, data=json.dumps(payload,indent=4))       
    
    assert resp.status_code == 200
    resp_body = resp.json()    
    print(resp_body, resp.text)


def test_attach_payment_method():
    url = URL+'attach-payment-method'
    print(url)
    headers = {'Content-Type': 'application/json' } 

    payload = {
    "payment_method_id": "pm_1I0F1RLptZvnIfh1l015lrr9",
    "customer_id": 'cus_IbRvbMA5rawp1o'
  	}
    
    resp = requests.post(url, headers=headers, data=json.dumps(payload,indent=4))       
    
    assert resp.status_code == 200
    resp_body = resp.json()    
    print(resp_body, resp.text)

# fails because subscription already exists
def test_create_subscription():
    url = URL+'create-subscription'
    print(url)
    headers = {'Content-Type': 'application/json' } 

    payload = {
    "plan_id": "plan_IbRkRcsOk7wdEq",
    "customer_id": 'cus_IbRvbMA5rawp1o'
  	}
    
    resp = requests.post(url, headers=headers, data=json.dumps(payload,indent=4))       
    

    print(resp)
    #assert resp.status_code == 200
    resp_body = resp.json()    
    print(resp_body, resp.text)

test_create_subscription()