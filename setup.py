import stripe

stripe.api_key = "pk_test_51HzgdoLptZvnIfh1FeCs9VqfKYBmEhecKCFlTONd4oQjbTgCMJAFWrRCwO6ohKjg6RpJwrzRP1asB0D0UiOLCU4s00JJYR7bWP"
stripe.api_key = "sk_test_51HzgdoLptZvnIfh1eWQCsFqYIhdpFWSbl6Q6DaSzMcrppsfdkwo6j9ggXe6ivVxR589U8Mrp23NQIKbVSaBVRmIZ00KixMyz4N"


# create product
product = stripe.Product.create(
  name='MyWallSt Options Subscription',
)

# create price for product
plan = stripe.Plan.create(
  amount=89,
  currency="eur",
  interval="month",
  product=product["id"],
)

###

# create payment method
pm = stripe.PaymentMethod.create(
  type="card",
  card={
    "number": "4242424242424242",
    "exp_month": 12,
    "exp_year": 2021,
    "cvc": "314",
  },
)

# create customer
customer = stripe.Customer.create(
            email="vat@gmail.com",
        )

# attach payment method to customer
update = stripe.PaymentMethod.attach(
            pm["id"],
            customer=customer['id'],
        )

# make payment method default to a customer
stripe.Customer.modify(
            customer['id'],
            invoice_settings={
                'default_payment_method': pm['id']
            }
        )

# create subscription
subscription = stripe.Subscription.create(
            customer=customer['id'],
            items=[
                {
                    "price": plan['id'],
                },
            ],
            expand=["latest_invoice.payment_intent"]
        )